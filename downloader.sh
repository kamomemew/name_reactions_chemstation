#!/bin/bash

for i in `seq 1 35`;do
    wget -nv "http://www.chem-station.com/odos/page/$i" -O "page$i" &
done

wait

for i in `seq 1 35`;do
    python3 ./get_article.py ./page$i >> integra.md
done

cat ./integra.md | grep "酸化" > oxidation.md
cat ./integra.md | grep "還元" > reduction.md
cat ./integra.md | grep "カップリング" > coupling.md

rm page*
