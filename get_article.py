#!/usr/bin/python3
#coding:utf-8
import sys
import lxml.html

f=open(sys.argv[1],"r")
html=f.read()
dom = lxml.html.fromstring(html)
for y in dom.xpath('/html/body/div[@id="contents"]/div[@id="main_col"]/ol[@id="archive_post_list"]/li'):
    print(
    "[{m}]({a})  ".format(
                         m=y.find("./h4/a").text.replace("[","\\[").replace("]","\\]"),
                         a=y.find("./h4/a").attrib["href"].replace("[","\\[").replace("]","\\]")
                         )
    )
print("****")
